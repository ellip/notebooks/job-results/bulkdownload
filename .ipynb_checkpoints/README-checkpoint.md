# Job Results Bulk-Download

## P-SBAS Bulk-Download
The instructions to generate the environment for the P-SBAS Jupyter Notebook are saved in the Terradue Knowledge Page https://knowledge.terradue.com/display/ELLIP/GEP+processing+service+-+fast+download+of+results+from+multiple+jobs.